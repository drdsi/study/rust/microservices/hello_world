use actix_web::{get, App, HttpServer, HttpResponse, Responder};
//use chrono;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(healthcheck)
    })
    .bind("0.0.0.0:8000")?
    .run()
    .await
}

#[get("/api/health")]
async fn healthcheck() -> impl Responder {
    HttpResponse::Ok().body("Hello World")
}